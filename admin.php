<?php 
ini_set('display_errors', '1');
define("ADMIN", true);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Widgets</title>
	<link rel="stylesheet" href="style.css"/>
	<link rel="stylesheet" href="jqui/jquery-ui.css"/>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="jqui/jquery-ui.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

	<script>
	jQuery(document).ready(function($) {

		$(".block").sortable({items: ".widget-block",update: function( event, ui ) {
			var widget = ui.item;
			$( widget ).attr("data-last-sort", widget.attr("data-sort") );
			var nameBlock = $( this ).attr("data-name");
			var idWidget = widget.attr("data-id");
			var sortWidget = $( this ).find(".widget-block");
			var i = 1;
			$( sortWidget ).each(function() {
				$( this ).attr( "data-sort", i);
				i++;
			});
			var LastSortWidget = $( this ).find(".widget-block[data-last-sort]");

				$.ajax({
					type: "POST",
					url: "sort_widget.php",
					data: {nameBlock: nameBlock,
						   idWidget: idWidget,
							sw:LastSortWidget.attr("data-sort"),
							lsw:LastSortWidget.attr("data-last-sort")},
					success: function(msg){
						// Some action
						//console.log(msg)
					}
				});
			$( LastSortWidget ).removeAttr("data-last-sort");

		}});
		//$(".block").disableSelection();

		$( ".widget" ).draggable({snap: ".block",snapMode: "inner", revert: "invalid", helper: "clone"});
		$( ".block" ).droppable({drop: function( event, ui ){
			if ( ui.draggable.hasClass( "widget" ) ){
				var widget = ui.draggable.css({left:0, top:0}).clone();
				var widgetTitle = ui.draggable.html();
				var nameBlock = $( this ).attr("data-name");
				var idWidget = widget.attr("data-id");
				var module = widget.attr("id");
				var sortWidget = $( this ).find(".widget-block").length + 1;
				$( this ).append( widget.removeClass("widget ui-draggable ui-draggable-handle").addClass("widget-block ui-sortable-handle").attr("data-sort",sortWidget).html(widgetTitle + "<div class=\"widget-close\" id=\"widget-close" + idWidget + "\">X</div>") );
				$.ajax({
					type: "POST",
					url: "add_widget.php",
					data: {nameBlock: nameBlock,
						   idWidget: idWidget,
						   sortWidget: sortWidget,
						   module: module},
					success: function(msg){
						// Some action
						var newId = msg;
						$( ".block[data-name='" + nameBlock + "']" ).find( ".widget-block[data-sort='" + sortWidget + "']" ).attr("data-index", newId );
						$( ".block[data-name='" + nameBlock + "']" ).find( ".widget-block[data-sort='" + sortWidget + "']" ).load();
					}
				});
			}
			}
		});

		$("body").on("click", ".widget-close", function() {
			var block = $( this ).parent().parent();
			var nameBlock = $( this ).parent().parent().attr("data-name");
			var idWidget = $( this ).parent().attr("data-index");
			var sortWidget = $( this ).parent().attr("data-sort");
			$( this ).parent().remove();
			var afterDel = block.find(".widget-block");
			var i = 1;
			$( afterDel ).each(function() {
				$( this ).attr( "data-sort", i);
				i++;
			});

			$.ajax({
					type: "POST",
					url: "del_widget.php",
					data: {nameBlock: nameBlock,
						   idWidget: idWidget,
						   sortWidget: sortWidget},
					success: function(msg){
						// Some action
					}
				});
		});

		$("body").on("click", "form input[type='submit']", function() {
			var module = $( this ).parent().attr("data-module");
			var idWidget = $( this ).parent().attr("data-id");
			var url = $( this ).parent().find("input[class='url']").val();
			if ( module == "text" ){
				var text = $( this ).parent().find("textarea").val();
			}
			else if( module == "map" ){
				var text = $( this ).parent().find("input[type='textbox']:not(.url)").val();
			}
			else if( module == "image" ){
				var text = $( this ).parent().find("input[type='textbox']:not(.url)").val();
			}
				$.ajax({
					type: "POST",
					url: "edit_widget.php",
					data: {idWidget: idWidget,
						   module: module,
						   text: text,
						   url: url},
					success: function(msg){
						// Some action
						$( ".widget-block[data-index='" + idWidget + "']" ).append( "<div class='done'>" + msg + "</div>");
						setInterval( function(){
							$(".done").fadeOut('fast');
						}, 2000 )
					}
				});
			console.log( module + " " + idWidget + " " + text);
			return false;
		});
	});
	</script>
</head>
<body>
<?
include_once($_SERVER['DOCUMENT_ROOT']."/dbconnect.php");
include_once($_SERVER['DOCUMENT_ROOT']."/func.php");
$sql_b = "SELECT * FROM block";
$data_b = mysql_query( $sql_b, $link ) or die( mysql_error() );
$page = $_REQUEST["page"];
?>
<div class="content-wpr">
<div class="header row">
	<div data-name="header" class="block col-sm-12">
		<h2><?=getTitle(header, $link)?></h2>
		<ul class="nav navbar-nav">
			<li><a href="/?page=page1">Page 1</a></li>
			<li><a href="/?page=page2">Page 2</a></li>
			<li><a href="/?page=page3">Page 3</a></li>
			<li><a href="/">USER</a></li>
			<li><a href="/admin.php">ADMIN</a></li>
		</ul>
		<?=getWidgets(header, $link, $page)?>
	</div>
</div>
<div class="content row">
	<div data-name="content" class="block left col-sm-8">
		<h2><?=getTitle(content, $link)?></h2>
		<?=getWidgets(content, $link, $page)?>
	</div>
	<div data-name="sidebar" class="block right col-sm-4">
		<h2><?=getTitle(sidebar, $link)?></h2>
		<?=getWidgets(sidebar, $link, $page)?>
	</div>
</div>
<div class="prefooter row">
	<div data-name="block1"  class="block pf col-sm-4">
		<h2><?=getTitle(block1, $link)?></h2>
		<?=getWidgets(block1, $link, $page)?>
	</div>
	<div data-name="block2" class="block pf col-sm-4">
		<h2><?=getTitle(block2, $link)?></h2>
		<?=getWidgets(block2, $link, $page)?>
	</div>
	<div data-name="block3" class="block pf col-sm-4">
		<h2><?=getTitle(block3, $link)?></h2>
		<?=getWidgets(block3, $link, $page)?>
	</div>
</div>
<div class="footer row">
	<div data-name="footer" class="block col-sm-12">
		<h2><?=getTitle(footer, $link)?></h2>
		<?=getWidgets(footer, $link, $page)?>
	</div>
</div>
	<div class="admin-widgets">
		<h2><?=getTitle(widgets, $link)?></h2>
		<?=getAllWidgets( $link )?>
	</div>
</div>
</body>
</html>