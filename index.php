<?php 
ini_set('display_errors', '1');
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Widgets</title>
	<link rel="stylesheet" href="style.css"/>
	<link rel="stylesheet" href="jqui/jquery-ui.css"/>
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="jqui/jquery-ui.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
</head>
<body>
<?
include_once($_SERVER['DOCUMENT_ROOT']."/dbconnect.php");
include_once($_SERVER['DOCUMENT_ROOT']."/func.php");
$sql_b = "SELECT * FROM block";
$data_b = mysql_query( $sql_b, $link ) or die( mysql_error() );
$page = $_REQUEST["page"];

?>
<div class="content-wpr">
<div class="header row">
	<div data-name="header" class="block col-sm-12">
		<h2><?=getTitle(header, $link)?></h2>
		<ul class="nav navbar-nav">
        	<li><a href="/?page=page1">Page 1</a></li>
        	<li><a href="/?page=page2">Page 2</a></li>
        	<li><a href="/?page=page3">Page 3</a></li>
			<li><a href="/">USER</a></li>
			<li><a href="/admin.php">ADMIN</a></li>
    	</ul>
		<?=getWidgets(header, $link, $page)?>
	</div>
</div>
<div class="content row">
	<div data-name="content" class="block left col-sm-8">
		<h2><?=getTitle(content, $link)?></h2>
		<?=getWidgets(content, $link, $page)?>
	</div>
	<div data-name="sidebar" class="block right col-sm-4">
		<h2><?=getTitle(sidebar, $link)?></h2>
		<?=getWidgets(sidebar, $link, $page)?>
	</div>
</div>
<div class="prefooter row">
	<div data-name="block1"  class="block pf col-sm-4">
		<h2><?=getTitle(block1, $link)?></h2>
		<?=getWidgets(block1, $link, $page)?>
	</div>
	<div data-name="block2" class="block pf col-sm-4">
		<h2><?=getTitle(block2, $link)?></h2>
		<?=getWidgets(block2, $link, $page)?>
	</div>
	<div data-name="block3" class="block pf col-sm-4">
		<h2><?=getTitle(block3, $link)?></h2>
		<?=getWidgets(block3, $link, $page)?>
	</div>
</div>
<div class="footer row">
	<div data-name="footer" class="block col-sm-12">
		<h2><?=getTitle(footer, $link)?></h2>
		<?=getWidgets(footer, $link, $page)?>
	</div>
</div>
</div>
</body>
</html>